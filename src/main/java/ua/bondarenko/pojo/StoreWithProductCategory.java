package ua.bondarenko.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class StoreWithProductCategory {
    private String storeAddress;
    private String storeName;
    private String categoryName;
    private int amountOfCategory;
}
