package ua.bondarenko.pojo;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.bson.BsonValue;

@Getter
@Setter
@Accessors(chain = true)
public class ProductInStore {

    @NotNull(message = "Store id should be not null")
    private BsonValue storeId;

    @NotNull(message = "Product id should be not null")
    private BsonValue productId;

    @Min(value = 1, message = "Amount should be {value} or more")
    private int amount;
}
