package ua.bondarenko;

import ua.bondarenko.pojo.ProductInStore;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ProductInStoreValidateGenerator implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInStoreValidateGenerator.class);

    private final int numberOfProductInStoreToGenerate;
    private final BlockingQueue<ProductInStore> productInStoreQueue;
    private final ProductInStoreGenerator generator;
    private final ProductInStoreValidator validator;

    @Override
    public void run() {
        int limit = Math.max(numberOfProductInStoreToGenerate, 0);
        LOGGER.info("Product in store validate generator start working");
        Stream.generate(generator::generate)
              .filter(validator::isValid)
              .limit(limit)
              .forEach(productInStoreQueue::add);
        LOGGER.info("Product in store validate generator finish and generate {} objects", limit);
    }
}
