package ua.bondarenko.dao;

import com.mongodb.client.MongoClient;
import org.bson.Document;
import ua.bondarenko.converter.Converter;
import ua.bondarenko.exception.DatabaseConnectionException;
import ua.bondarenko.pojo.ProductInStore;

public class DaoFactory {

    public MongoDao getMongoDao(MongoClient mongoClient,
                                String dbName,
                                Converter<ProductInStore, Document> converter) {
        if (mongoClient == null) {
            throw new DatabaseConnectionException("Database client should be not null!");
        }

        if (dbName == null) {
            throw new DatabaseConnectionException("Database name should be not null!");
        }

        if (converter == null) {
            throw new IllegalArgumentException("Converter should be not null");
        }

        return new MongoDao(mongoClient, dbName, converter);
    }
}
