package ua.bondarenko.dao;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.InsertManyResult;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.bondarenko.converter.Converter;
import ua.bondarenko.pojo.ProductInStore;
import ua.bondarenko.pojo.StoreWithProductCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Projections.*;

public class MongoDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(MongoDao.class);
    private static final String STORE_COLLECTION_NAME = "store";
    private static final String PRODUCT_CATEGORY_COLLECTION_NAME = "product-category";
    private static final String PRODUCT_COLLECTION_NAME = "product";
    private static final String PRODUCT_IN_STORE_COLLECTION_NAME = "product-in-store";
    private static final String CATEGORY_DEFAULT_PREFIX = "category ";
    private static final String PRODUCT_DEFAULT_PREFIX = "product ";
    private static final String STORE_NAME_DEFAULT_PREFIX = "store ";
    private static final String ADDRESS_DEFAULT_PREFIX = "address ";
    private final Converter<ProductInStore, Document> converter;
    private final MongoCollection<Document> categoryCollection;
    private final MongoCollection<Document> productCollection;
    private final MongoCollection<Document> storeCollection;
    private final MongoCollection<Document> productInStoreCollection;

    public MongoDao(MongoClient mongoClient,
                    String dbName,
                    Converter<ProductInStore, Document> converter) {
        this.converter = converter;

        MongoDatabase database = mongoClient.getDatabase(dbName);

        this.categoryCollection = database.getCollection(PRODUCT_CATEGORY_COLLECTION_NAME);
        this.productCollection = database.getCollection(PRODUCT_COLLECTION_NAME);
        this.storeCollection = database.getCollection(STORE_COLLECTION_NAME);
        this.productInStoreCollection = database.getCollection(PRODUCT_IN_STORE_COLLECTION_NAME);
    }

    public List<BsonValue> addProductCategories(int numberOfCategories,
                                                int batchMaxSize) {
        List<BsonValue> categoryIds = new ArrayList<>(numberOfCategories);
        List<Document> documents = new ArrayList<>(batchMaxSize);

        IntStream.range(1, numberOfCategories + 1)
                .forEach(categoryNumber -> {
                    documents.add(new Document().append("name", CATEGORY_DEFAULT_PREFIX + categoryNumber));
                    if (documents.size() == batchMaxSize || categoryNumber == numberOfCategories) {
                        InsertManyResult result = categoryCollection.insertMany(documents);
                        categoryIds.addAll(result.getInsertedIds().values());
                        documents.clear();
                    }
                });
        return categoryIds;
    }

    public List<BsonValue> addProducts(List<BsonValue> categoryIds,
                                       int numberOfProductsPerCategory) {
        AtomicInteger productIdCounter = new AtomicInteger(1);
        List<BsonValue> productIds = new ArrayList<>();
        List<Document> documents = new ArrayList<>(numberOfProductsPerCategory);

        categoryIds.forEach(categoryId -> {
            IntStream.range(productIdCounter.get(), productIdCounter.get() + numberOfProductsPerCategory)
                    .forEach(productNumber -> documents.add(new Document().append("name", PRODUCT_DEFAULT_PREFIX + productNumber)
                                                                          .append("category_id", categoryId)));
            InsertManyResult result = productCollection.insertMany(documents);
            productIds.addAll(result.getInsertedIds().values());
            documents.clear();
            productIdCounter.addAndGet(numberOfProductsPerCategory);
        });

        return productIds;
    }

    public List<BsonValue> addStores(int numberOfStores,
                                     int batchMaxSize) {
        List<BsonValue> storeIds = new ArrayList<>(numberOfStores);
        List<Document> documents = new ArrayList<>(batchMaxSize);

        IntStream.range(1, numberOfStores + 1)
                .forEach(storeNumber -> {
                    documents.add(new Document().append("name", STORE_NAME_DEFAULT_PREFIX + storeNumber)
                                                .append("address", ADDRESS_DEFAULT_PREFIX + storeNumber));
                    if (documents.size() == batchMaxSize || storeNumber == numberOfStores) {
                        InsertManyResult result = storeCollection.insertMany(documents);
                        storeIds.addAll(result.getInsertedIds().values());
                        documents.clear();
                    }
                });
        return storeIds;
    }

    public int addAllProductInStore(List<ProductInStore> productInStores) {
        InsertManyResult result = productInStoreCollection.insertMany(converter.convertAll(productInStores));
        return result.getInsertedIds().size();
    }

    public void createIndexes() {
        categoryCollection.createIndex(Indexes.ascending("category"));
        productCollection.createIndex(Indexes.ascending("category_id"));
        productInStoreCollection.createIndex(Indexes.ascending("product_id"));
        LOGGER.debug("Indexes was successfully created!");
    }

    public StoreWithProductCategory getStoreWithMaxNumberOfProductsByCategory(String category) {

        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category);
        Document categoryDoc = categoryCollection.find(Filters.eq("name", category)).first();
        if (categoryDoc == null || categoryDoc.isEmpty()) {
            return null;
        }
        ObjectId categoryId = categoryDoc.getObjectId("_id");
        LOGGER.debug("Category id = {}", categoryId);

        FindIterable<Document> productDocs = productCollection.find(Filters.eq("category_id", categoryId));
        List<ObjectId> productIds = StreamSupport.stream(productDocs.spliterator(), false)
                                                 .map(doc -> doc.getObjectId("_id"))
                                                 .collect(Collectors.toList());
        LOGGER.debug("Number of products by category = {}", productIds.size());

        List<Bson> agregateList = List.of(match(Filters.in("product_id", productIds)),
                                          group("$store_id", Accumulators.sum("amount", "$amount")),
                                          sort(Sorts.descending("amount")));
        Document maxAmountStore = productInStoreCollection.aggregate(agregateList).first();
        if (maxAmountStore == null || maxAmountStore.isEmpty()) {
            return null;
        }

        ObjectId storeId = maxAmountStore.getObjectId("_id");
        Document store = storeCollection.find(Filters.eq("_id", storeId)).first();
        if (store == null || store.isEmpty()) {
            return null;
        }

        return storeWithProductCategory.setStoreName(store.getString("name"))
                                       .setStoreAddress(store.getString("address"))
                                       .setAmountOfCategory(maxAmountStore.getInteger("amount"));
    }

    public StoreWithProductCategory getWithLookUp(String category) {

        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category);

        List<Bson> agregateList = List.of(match(Filters.in("name", category)),
                                          lookup(PRODUCT_COLLECTION_NAME, "_id", "category_id", "product"),
                                          unwind("$product"),
                                          lookup(PRODUCT_IN_STORE_COLLECTION_NAME, "product._id", "product_id", "pis"),
//                                          unset("product"),
                                          unwind("$pis"),
                                          group("$pis.store_id", Accumulators.sum("amount", "$pis.amount")),
                                          sort(Sorts.descending("amount")),
                                          limit(1),
                                          lookup(STORE_COLLECTION_NAME, "_id", "_id", "store"),
                                          unwind("$store"),
                                          project(fields(include("amount", "store.name", "store.address"), excludeId())));

        Document storeWithAmount = categoryCollection.aggregate(agregateList).first();
        if (storeWithAmount == null || storeWithAmount.isEmpty()) {
            return null;
        }
        Document store = storeWithAmount.get("store", Document.class);
        return storeWithProductCategory.setStoreName(store.getString("name"))
                .setStoreAddress(store.getString("address"))
                .setAmountOfCategory(storeWithAmount.getInteger("amount"));
    }

    public void dropCollections() {
        productInStoreCollection.drop();
        storeCollection.drop();
        productCollection.drop();
        categoryCollection.drop();
    }
}
