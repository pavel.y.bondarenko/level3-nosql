package ua.bondarenko;

import ua.bondarenko.pojo.ProductInStore;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

public class ProductInStoreValidator {
    private final Validator validator;

    public ProductInStoreValidator() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            this.validator = factory.getValidator();
        }
    }

    public boolean isValid(ProductInStore productInStore) {
        return validator.validate(productInStore).isEmpty();
    }
}
