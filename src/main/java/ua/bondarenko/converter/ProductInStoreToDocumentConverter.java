package ua.bondarenko.converter;

import org.bson.Document;
import ua.bondarenko.pojo.ProductInStore;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ProductInStoreToDocumentConverter implements Converter<ProductInStore, Document> {

    @Override
    public Document convert(ProductInStore productInStore) {
        return new Document().append("store_id", productInStore.getStoreId())
                             .append("product_id", productInStore.getProductId())
                             .append("amount", productInStore.getAmount());
    }

    @Override
    public List<Document> convertAll(Collection<ProductInStore> collection) {
        return collection.stream().map(this::convert).collect(Collectors.toList());
    }
}
