package ua.bondarenko.converter;

import java.util.Collection;
import java.util.List;

public interface Converter<T, V> {
    V convert(T value);
    List<V> convertAll(Collection<T> values);
}
