package ua.bondarenko;

import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.pojo.ProductInStore;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class ExecutorCreator {

    public ExecutorService createGeneratorValidatorExecutor(int numberOfThreads,
                                                            int totalNumberOfObjects,
                                                            BlockingQueue<ProductInStore> queueToAdd,
                                                            ProductInStoreGenerator generator,
                                                            ProductInStoreValidator validator) {
        ExecutorService executorService = Executors.newFixedThreadPool(Math.max(numberOfThreads, 1));
        int numberOfObjects = totalNumberOfObjects / numberOfThreads;
        numberOfThreads = Math.max(numberOfThreads, 0);

        for (int i = 0; i < numberOfThreads; i++) {
            if (i == (numberOfThreads - 1)) {
                numberOfObjects += totalNumberOfObjects % numberOfThreads;
            }

            executorService.execute(new ProductInStoreValidateGenerator(numberOfObjects,
                                                                        queueToAdd,
                                                                        generator,
                                                                        validator));
        }

        return executorService;
    }

    public ExecutorService createBatchSenderExecutor(int numberOfThreads,
                                                     BlockingQueue<ProductInStore> queueToPoll,
                                                     DaoFactory daoFactory,
                                                     String dbUrl,
                                                     String dbName,
                                                     int batchMaxSize) {
        ExecutorService executorService = Executors.newFixedThreadPool(Math.max(numberOfThreads, 1));

        numberOfThreads = Math.max(numberOfThreads, 0);
        Stream.generate(() -> new ProductInStoreBatchSender(queueToPoll, daoFactory, dbUrl, dbName, batchMaxSize))
              .limit(numberOfThreads)
              .forEach(executorService::execute);
        return executorService;
    }
}
