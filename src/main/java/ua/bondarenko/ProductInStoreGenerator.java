package ua.bondarenko;

import org.bson.BsonValue;
import ua.bondarenko.pojo.ProductInStore;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public class ProductInStoreGenerator {

    private final List<BsonValue> storeIds;
    private final List<BsonValue> productIds;
    private final Random random;
    private final int maxAmountOfProducts;

    public ProductInStore generate() {
        int productIndex = random.ints(0, productIds.size()).findFirst().getAsInt();
        int storeIndex = random.ints(0, storeIds.size()).findFirst().getAsInt();
        int amount = random.ints(1, maxAmountOfProducts + 1).findFirst().getAsInt();

        return new ProductInStore().setProductId(productIds.get(productIndex))
                                   .setStoreId(storeIds.get(storeIndex))
                                   .setAmount(amount);
    }
}
