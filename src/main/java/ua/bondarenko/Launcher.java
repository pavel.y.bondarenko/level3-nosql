package ua.bondarenko;

import ua.bondarenko.converter.ProductInStoreToDocumentConverter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.exception.PropertyFileDoesNotExistsException;
import ua.bondarenko.exception.PropertyMissedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

public class Launcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);
    private static final String PROPERTY_FILE_NAME = "app.properties";


    public static void main(String[] args) {
        PropertyService propertyService;
        try {
            propertyService = new PropertyService(PROPERTY_FILE_NAME);
        } catch (PropertyFileDoesNotExistsException | PropertyMissedException e) {
            LOGGER.error("Some problems with properties", e);
            return;
        }

        if (propertyService.getTruststore() != null) {
            System.setProperty("javax.net.ssl.trustStore", propertyService.getTruststore());
        }

        if (propertyService.getTruststorePassword() != null) {
            System.setProperty("javax.net.ssl.trustStorePassword", propertyService.getTruststorePassword());
        }

        new App(propertyService,
                new ProductInStoreToDocumentConverter(),
                new ExecutorCreator(),
                new LinkedBlockingQueue<>(),
                new DaoFactory())
                .run();

        LOGGER.info("End of program!");
    }
}
