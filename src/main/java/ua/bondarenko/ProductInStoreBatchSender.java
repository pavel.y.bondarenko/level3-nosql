package ua.bondarenko;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import ua.bondarenko.converter.ProductInStoreToDocumentConverter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.dao.MongoDao;
import ua.bondarenko.pojo.ProductInStore;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

@RequiredArgsConstructor
public class ProductInStoreBatchSender implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInStoreBatchSender.class);


    private final BlockingQueue<ProductInStore> productInStoreQueue;
    private final DaoFactory daoFactory;
    private final String dbUrl;
    private final String dbName;
    private final int batchMaxSize;

    @Override
    public void run() {
        LOGGER.info("Batch sender start working!");
        try (MongoClient mongoClient = MongoClients.create(dbUrl)) {
            sendBatches(daoFactory.getMongoDao(mongoClient, dbName, new ProductInStoreToDocumentConverter()));
        } catch (Exception e) {
            LOGGER.warn("Something wrong with database", e);
        }
        LOGGER.info("Sender finish!");
    }

    private void sendBatches(MongoDao mongoDao) {
        boolean hasNoPoisonObject = true;
        List<ProductInStore> productInStores = new ArrayList<>(batchMaxSize);
        while (hasNoPoisonObject) {
            while (productInStores.size() < batchMaxSize && hasNoPoisonObject) {
                ProductInStore productInStore = productInStoreQueue.poll();
                if (productInStore != null) {
                    if (productInStore.getStoreId() != null) {
                        productInStores.add(productInStore);
                    } else {
                        hasNoPoisonObject = false;
                        LOGGER.info("Poison object obtained!");
                    }
                }
            }

            if (!productInStores.isEmpty()) {
                int numberOfDocumentsSaved = mongoDao.addAllProductInStore(productInStores);
                productInStores.clear();
                LOGGER.info("Batch with {} messages sent", numberOfDocumentsSaved);
            }
        }
    }
}
