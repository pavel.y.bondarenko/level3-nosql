package ua.bondarenko;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.BsonValue;
import org.bson.Document;
import ua.bondarenko.converter.Converter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.dao.MongoDao;
import ua.bondarenko.exception.DatabaseConnectionException;
import ua.bondarenko.pojo.ProductInStore;
import ua.bondarenko.pojo.StoreWithProductCategory;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private final PropertyService propertyService;
    private final Converter<ProductInStore, Document> converter;
    private final ExecutorCreator executorCreator;
    private final BlockingQueue<ProductInStore> productInStoreQueue;
    private final DaoFactory daoFactory;

    public void run() {
        try (MongoClient mongoClient = MongoClients.create(propertyService.getDbUrl())) {
            MongoDao mongoDao = daoFactory.getMongoDao(mongoClient, propertyService.getDbName(), converter);
            mongoDao.dropCollections();
            int batchMaxSize = propertyService.getBatchMaxSize();
            List<BsonValue> categoryIds = mongoDao.addProductCategories(propertyService.getNumberOfCategories(),
                                                                        batchMaxSize);

            List<BsonValue> productIds = mongoDao.addProducts(categoryIds, propertyService.getNumberOfProductsPerCategory());

            List<BsonValue> storeIds = mongoDao.addStores(propertyService.getNumberOfStores(), batchMaxSize);

            ProductInStoreGenerator generator = new ProductInStoreGenerator(storeIds,
                                                                            productIds,
                                                                            new Random(),
                                                                            propertyService.getMaxAmountOfProducts());
            ProductInStoreValidator validator = new ProductInStoreValidator();

            StopWatch watch = new StopWatch();
            watch.start();

            ExecutorService generatorValidatorsExecutor = executorCreator.createGeneratorValidatorExecutor(propertyService.getNumberOfGenerators(),
                                                                                                           propertyService.getNumberOfProductInStore(),
                                                                                                           productInStoreQueue,
                                                                                                           generator,
                                                                                                           validator);
            ExecutorService batchSendersExecutor = executorCreator.createBatchSenderExecutor(propertyService.getNumberOfSenders(),
                                                                                             productInStoreQueue,
                                                                                             daoFactory,
                                                                                             propertyService.getDbUrl(),
                                                                                             propertyService.getDbName(),
                                                                                             batchMaxSize);

            waitExecutor(generatorValidatorsExecutor);

            Stream.generate(ProductInStore::new)
                  .limit(propertyService.getNumberOfSenders())
                  .forEach(productInStoreQueue::add);

            waitExecutor(batchSendersExecutor);

            watch.stop();
            LOGGER.info("Time of saving {} raws in DB = {} ms", propertyService.getNumberOfProductInStore(), watch.getTime());

            watch.reset();
            watch.start();
            StoreWithProductCategory storeWithProductCategory = mongoDao.getStoreWithMaxNumberOfProductsByCategory(propertyService.getCategory());
            if (storeWithProductCategory == null) {
                LOGGER.error("Can not find category");
                return;
            }
            LOGGER.info(createOutMessage(storeWithProductCategory));
            watch.stop();
            LOGGER.info("Find store WITHOUT indexes for {} ms", watch.getTime());

            try {
                watch.reset();
                watch.start();
                StoreWithProductCategory withLookUp = mongoDao.getWithLookUp(propertyService.getCategory());
                if (withLookUp == null) {
                    LOGGER.error("Can not find category");
                    return;
                }
                LOGGER.info(createOutMessage(withLookUp));
                watch.stop();
                LOGGER.info("Find store WITH LOOK UP WITHOUT  indexes for {} ms", watch.getTime());
            } catch (Exception e) {
                LOGGER.warn("Some exception WITH LOOK UP WITHOUT indexes!!!");
            }

            watch.reset();
            watch.start();
            mongoDao.createIndexes();
            watch.stop();
            LOGGER.info("Indexing for {} ms", watch.getTime());

            watch.reset();
            watch.start();
            StoreWithProductCategory storeWithProductCategory2 = mongoDao.getStoreWithMaxNumberOfProductsByCategory(propertyService.getCategory());
            if (storeWithProductCategory2 == null) {
                LOGGER.error("Can not find category");
                return;
            }
            LOGGER.info(createOutMessage(storeWithProductCategory2));
            watch.stop();
            LOGGER.info("Find store WITH indexes for {} ms", watch.getTime());

            watch.reset();
            watch.start();
            StoreWithProductCategory withLookUp2 = mongoDao.getWithLookUp(propertyService.getCategory());
            if (withLookUp2 == null) {
                LOGGER.error("Can not find category");
                return;
            }
            LOGGER.info(createOutMessage(withLookUp2));
            watch.stop();
            LOGGER.info("Find store WITH LOOK UP WITH  indexes for {} ms", watch.getTime());

        } catch (DatabaseConnectionException e) {
            LOGGER.error("Connection failed", e);
        } catch (IllegalArgumentException | IllegalStateException e) {
            LOGGER.error("There are some troubles with properties", e);
        }
    }

    private void waitExecutor(ExecutorService executorService) {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(propertyService.getMaxWaitingTimeInSeconds(), TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.warn("There is exception with executor shutdown", e);
            executorService.shutdownNow();
        }
    }

    private String createOutMessage(StoreWithProductCategory storeWithProductCategory) {
        return String.format(propertyService.getOutFormat(),
                             storeWithProductCategory.getCategoryName(),
                             storeWithProductCategory.getAmountOfCategory(),
                             storeWithProductCategory.getStoreName(),
                             storeWithProductCategory.getStoreAddress());
    }
}
