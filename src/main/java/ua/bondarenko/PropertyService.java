package ua.bondarenko;

import org.apache.commons.lang3.StringUtils;
import ua.bondarenko.exception.PropertyFileDoesNotExistsException;
import ua.bondarenko.exception.PropertyMissedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyService.class);
    private static final Charset PROPERTY_ENCODING = StandardCharsets.UTF_8;
    private static final String URL_PROPERTY_NAME = "dbURL";
    private static final String DB_NAME_PROPERTY_NAME = "dbName";
    private static final String NUMBER_OF_CATEGORIES_PROPERTY_NAME = "number-of-categories";
    private static final String NUMBER_OF_PRODUCTS_PER_CATEGORY_PROPERTY_NAME = "number-of-products-per-category";
    private static final String NUMBER_OF_STORES_PROPERTY_NAME = "number-of-stores";
    private static final String NUMBER_OF_PRODUCTS_IN_STORE_PROPERTY_NAME = "number-of-product-in-store";
    private static final String BATCH_MAX_SIZE_PROPERTY_NAME = "batch-max-size";
    private static final String MAX_AMOUNT_OF_PRODUCTS_PROPERTY_NAME = "max-amount-of-products";
    private static final String NUMBER_OF_GENERATORS_PROPERTY_NAME = "number-of-generators";
    private static final String NUMBER_OF_SENDERS_PROPERTY_NAME = "number-of-senders";
    private static final String MAX_WAITING_TIME_PROPERTY_NAME = "max-waiting-time-in-seconds";
    private static final String OUT_FORMAT_PROPERTY_NAME = "out-format";
    private static final String CATEGORY_PROPERTY_NAME = "category";
    private static final String TRUSTSTORE_PROPERTY_NAME = "truststore";
    private static final String TRUSTSTORE_PASSWORD_PROPERTY_NAME = "truststorePassword";
    private static final String PROPERTY_IS_MISSED_TEMPLATE = "Property '{}' is missed!";
    private static final int DEFAULT_NUMBER_OF_CATEGORIES = 100;
    private static final int DEFAULT_NUMBER_OF_PRODUCTS_PER_CATEGORY = 100;
    private static final int DEFAULT_NUMBER_OF_STORES = 100;
    private static final int DEFAULT_NUMBER_OF_PRODUCTS_IN_STORE = 3_000_000;
    private static final int DEFAULT_BATCH_MAX_SIZE = 3_000;
    private static final int DEFAULT_MAX_AMOUNT_OF_PRODUCTS = 1_000;
    private static final int DEFAULT_NUMBER_OF_GENERATORS = 1;
    private static final int DEFAULT_NUMBER_OF_SENDERS = 3;
    private static final int DEFAULT_MAX_WAITING_TIME_IN_SECONDS = 600;
    private static final String DEFAULT_OUT_FORMAT = "Товару категорії '%s' більше всього (%d шт.) у магазині '%s' за адресою: %s";

    private final Properties properties;

    public PropertyService(String fileName) {
        properties = getProperties(fileName);
        if (isSomePropertyMissed()) {
            throw new PropertyMissedException("Some property is missed!");
        }
    }

    public String getDbUrl() {
        return properties.getProperty(URL_PROPERTY_NAME);
    }

    public String getDbName() {
        return properties.getProperty(DB_NAME_PROPERTY_NAME);
    }

    public int getNumberOfCategories() {
        return getNumberProperty(NUMBER_OF_CATEGORIES_PROPERTY_NAME, DEFAULT_NUMBER_OF_CATEGORIES);
    }

    public int getNumberOfProductsPerCategory() {
        return getNumberProperty(NUMBER_OF_PRODUCTS_PER_CATEGORY_PROPERTY_NAME, DEFAULT_NUMBER_OF_PRODUCTS_PER_CATEGORY);
    }

    public int getNumberOfStores() {
        return getNumberProperty(NUMBER_OF_STORES_PROPERTY_NAME, DEFAULT_NUMBER_OF_STORES);
    }

    public int getNumberOfProductInStore() {
        return getNumberProperty(NUMBER_OF_PRODUCTS_IN_STORE_PROPERTY_NAME, DEFAULT_NUMBER_OF_PRODUCTS_IN_STORE);
    }

    public int getBatchMaxSize() {
        return getNumberProperty(BATCH_MAX_SIZE_PROPERTY_NAME, DEFAULT_BATCH_MAX_SIZE);
    }

    public int getMaxAmountOfProducts() {
        return getNumberProperty(MAX_AMOUNT_OF_PRODUCTS_PROPERTY_NAME, DEFAULT_MAX_AMOUNT_OF_PRODUCTS);
    }

    public int getNumberOfGenerators() {
        return getNumberProperty(NUMBER_OF_GENERATORS_PROPERTY_NAME, DEFAULT_NUMBER_OF_GENERATORS);
    }

    public int getNumberOfSenders() {
        return getNumberProperty(NUMBER_OF_SENDERS_PROPERTY_NAME, DEFAULT_NUMBER_OF_SENDERS);
    }

    public int getMaxWaitingTimeInSeconds() {
        return getNumberProperty(MAX_WAITING_TIME_PROPERTY_NAME, DEFAULT_MAX_WAITING_TIME_IN_SECONDS);
    }

    public String getOutFormat() {
        return properties.getProperty(OUT_FORMAT_PROPERTY_NAME, DEFAULT_OUT_FORMAT);
    }

    public String getCategory() {
        return System.getProperty(CATEGORY_PROPERTY_NAME);
    }

    public String getTruststore() {
        return properties.getProperty(TRUSTSTORE_PROPERTY_NAME);
    }

    public String getTruststorePassword() {
        return properties.getProperty(TRUSTSTORE_PASSWORD_PROPERTY_NAME);
    }

    private Properties getProperties(String fileName) throws PropertyFileDoesNotExistsException {
        Properties foundProperties = new Properties();

        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
            foundProperties.load(new InputStreamReader(resourceAsStream, PROPERTY_ENCODING));
            LOGGER.debug("Property file was loaded!");
        } catch (NullPointerException | IOException e) {
            throw new PropertyFileDoesNotExistsException(e);
        }

        return foundProperties;
    }

    private boolean isSomePropertyMissed() {
        boolean isMissed = false;

        if (StringUtils.isBlank(properties.getProperty(URL_PROPERTY_NAME))) {
            LOGGER.error(PROPERTY_IS_MISSED_TEMPLATE, URL_PROPERTY_NAME);
            isMissed = true;
        }

        if (StringUtils.isBlank(properties.getProperty(DB_NAME_PROPERTY_NAME))) {
            LOGGER.error(PROPERTY_IS_MISSED_TEMPLATE, DB_NAME_PROPERTY_NAME);
            isMissed = true;
        }

        if (StringUtils.isBlank(System.getProperty(CATEGORY_PROPERTY_NAME))) {
            LOGGER.error("System property '{}' is missed!", CATEGORY_PROPERTY_NAME);
            isMissed = true;
        }

        return isMissed;
    }

    private int getNumberProperty(String name, int defaultValue) {
        try {
            return Integer.parseInt(properties.getProperty(name).replace("_", ""));
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
