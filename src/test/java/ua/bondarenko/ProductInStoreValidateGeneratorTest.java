package ua.bondarenko;

import org.bson.BsonValue;
import ua.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.bondarenko.ProductInStoreGenerator;
import ua.bondarenko.ProductInStoreValidateGenerator;
import ua.bondarenko.ProductInStoreValidator;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreValidateGeneratorTest {
    private ProductInStoreValidateGenerator productInStoreValidateGenerator;
    private final int numberOfProductInStoreGenerate = 10;

    private final BlockingQueue<ProductInStore> productInStoreQueue = new LinkedBlockingQueue<>();

    @Mock
    private ProductInStoreGenerator generatorMock;

    @Mock
    private ProductInStoreValidator validatorMock;

    @Mock
    private BsonValue productIdMock;

    @Mock
    private BsonValue storeIdMock;

    @BeforeEach
    void setUp() {
        productInStoreValidateGenerator = new ProductInStoreValidateGenerator(numberOfProductInStoreGenerate,
                                                                              productInStoreQueue,
                                                                              generatorMock,
                                                                              validatorMock);
    }

    @AfterEach
    void tearDown() {
        productInStoreValidateGenerator = null;
    }

    @Test
    void runTest() {
        ProductInStore validProductInStore = new ProductInStore().setProductId(productIdMock)
                                                                 .setStoreId(storeIdMock)
                                                                 .setAmount(11);
        ProductInStore invalidProductInStore = new ProductInStore();

        when(generatorMock.generate()).thenReturn(invalidProductInStore, validProductInStore, invalidProductInStore, validProductInStore);
        when(validatorMock.isValid(validProductInStore)).thenReturn(true);
        when(validatorMock.isValid(invalidProductInStore)).thenReturn(false);

        productInStoreValidateGenerator.run();
        verify(generatorMock, times(numberOfProductInStoreGenerate + 2)).generate();
        verify(validatorMock, times(numberOfProductInStoreGenerate + 2)).isValid(any(ProductInStore.class));

        assertEquals(numberOfProductInStoreGenerate, productInStoreQueue.size());

        for (ProductInStore pis : productInStoreQueue) {
            assertEquals(validProductInStore, pis);
        }
    }
}