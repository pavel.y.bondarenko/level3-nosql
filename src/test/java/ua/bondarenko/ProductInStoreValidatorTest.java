package ua.bondarenko;

import org.bson.BsonValue;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.*;
import ua.bondarenko.ProductInStoreValidator;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreValidatorTest {
    private static ProductInStoreValidator productInStoreValidator;
    private ProductInStore productInStore;

    @Mock
    private BsonValue productIdMock;

    @Mock
    private BsonValue storeIdMock;

    @BeforeAll
    static void setAll() {
        productInStoreValidator = new ProductInStoreValidator();
    }

    @AfterAll
    static void clearAll() {
        productInStoreValidator = null;
    }

    @BeforeEach
    void setUp() {
        productInStore = new ProductInStore().setProductId(productIdMock)
                                             .setStoreId(storeIdMock)
                                             .setAmount(1);
    }

    @AfterEach
    void tearDown() {
        productInStore = null;
    }

    @Test
    void isValidCorrectTest() {
        assertTrue(productInStoreValidator.isValid(productInStore));
    }

    @Test
    void isValidStoreIdIsNullTest() {
        assertFalse(productInStoreValidator.isValid(productInStore.setStoreId(null)));
    }

    @Test
    void isValidProductIdIsNullTest() {
        assertFalse(productInStoreValidator.isValid(productInStore.setProductId(null)));
    }

    @Test
    void isValidIncorrectAmountTest() {
        int incorrectAmount = 0;
        assertFalse(productInStoreValidator.isValid(productInStore.setAmount(incorrectAmount)));
    }
}