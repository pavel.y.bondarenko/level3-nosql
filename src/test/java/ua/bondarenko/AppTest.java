package ua.bondarenko;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.Document;
import ua.bondarenko.converter.Converter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.dao.MongoDao;
import ua.bondarenko.exception.DatabaseConnectionException;
import ua.bondarenko.pojo.ProductInStore;
import ua.bondarenko.pojo.StoreWithProductCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppTest {

    private App app;

    @Mock
    private PropertyService propertyServiceMock;

    @Mock
    private ExecutorCreator executorCreatorMock;

    @Mock
    private MongoClient mongoClientMock;

    @Mock
    private DaoFactory daoFactoryMock;

    @Mock
    private MongoDao mongoDaoMock;

    @Mock
    private Converter<ProductInStore, Document> converterMock;

    @Mock
    private ExecutorService executorServiceWithGeneratorsMock;

    @Mock
    private ExecutorService executorServiceWithBatchSendersMock;

    private BlockingQueue<ProductInStore> queue = new LinkedBlockingQueue<>();

    @BeforeEach
    void setUp() {
        app = new App(propertyServiceMock,
                      converterMock,
                      executorCreatorMock,
                      queue,
                      daoFactoryMock);
    }

    @AfterEach
    void tearDown() {
        app = null;
    }

    @Test
    void runNormalTest() throws InterruptedException {
        String dbUrl = "url";
        String dbName = "name";
        String category = "category";
        String outFormat = "Товару категорії '%s' більше всього (%d шт.) у магазині '%s' за адресою: %s";
        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category)
                                                                                          .setStoreName("store")
                                                                                          .setStoreAddress("address")
                                                                                          .setAmountOfCategory(999);

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbName()).thenReturn(dbName);
        when(propertyServiceMock.getCategory()).thenReturn(category);
        when(propertyServiceMock.getOutFormat()).thenReturn(outFormat);
        when(mongoDaoMock.getStoreWithMaxNumberOfProductsByCategory(category)).thenReturn(storeWithProductCategory);
        when(mongoDaoMock.getWithLookUp(category)).thenReturn(storeWithProductCategory);

        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(dbUrl))
                               .thenReturn(mongoClientMock);
            when(daoFactoryMock.getMongoDao(mongoClientMock, dbName, converterMock))
                               .thenReturn(mongoDaoMock);
            when(executorCreatorMock.createGeneratorValidatorExecutor(anyInt(),
                                                                      anyInt(),
                                                                      any(),
                                                                      any(ProductInStoreGenerator.class),
                                                                      any(ProductInStoreValidator.class)))
                                    .thenReturn(executorServiceWithGeneratorsMock);
            when(executorCreatorMock.createBatchSenderExecutor(anyInt(),
                                                               any(),
                                                               any(DaoFactory.class),
                                                               anyString(),
                                                               anyString(),
                                                               anyInt()))
                                    .thenReturn(executorServiceWithBatchSendersMock);

            app.run();

            verify(mongoDaoMock, times(1)).addProductCategories(anyInt(), anyInt());
            verify(mongoDaoMock, times(1)).addProducts(any(), anyInt());
            verify(executorCreatorMock, times(1)).createGeneratorValidatorExecutor(anyInt(),
                                                                                                         anyInt(),
                                                                                                         any(),
                                                                                                         any(ProductInStoreGenerator.class),
                                                                                                         any(ProductInStoreValidator.class));
            verify(executorCreatorMock, times(1)).createBatchSenderExecutor(anyInt(),
                                                                                                   any(),
                                                                                                   any(DaoFactory.class),
                                                                                                   anyString(),
                                                                                                   anyString(),
                                                                                                   anyInt());
            verify(executorServiceWithGeneratorsMock, times(1)).shutdown();
            verify(executorServiceWithGeneratorsMock, times(1)).awaitTermination(anyLong(), any());
            verify(executorServiceWithBatchSendersMock, times(1)).shutdown();
            verify(executorServiceWithBatchSendersMock, times(1)).awaitTermination(anyLong(), any());
            verify(mongoDaoMock, times(2)).getStoreWithMaxNumberOfProductsByCategory(category);
            verify(mongoDaoMock, times(1)).createIndexes();
        }
    }

    @Test
    void runWithConnectionExceptionTest() {
        String dbUrl = "url";

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);

        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(dbUrl))
                    .thenThrow(IllegalStateException.class);
            assertDoesNotThrow(() -> app.run());
        }
    }

    @Test
    void runWithDatabaseConnectionExceptionTest() {
        String dbUrl = "url";
        String dbName = "name";

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbName()).thenReturn(dbName);

        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(dbUrl))
                               .thenReturn(mongoClientMock);
            when(daoFactoryMock.getMongoDao(mongoClientMock, dbName, converterMock))
                    .thenThrow(DatabaseConnectionException.class);
            assertDoesNotThrow(() -> app.run());
        }
    }

    @Test
    void runWithInterruptedExceptionTest() throws InterruptedException {
        String dbUrl = "url";
        String dbName = "name";
        String category = "category";
        String outFormat = "Товару категорії '%s' більше всього (%d шт.) у магазині '%s' за адресою: %s";
        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category)
                                                                                          .setStoreName("store-name")
                                                                                          .setAmountOfCategory(999)
                                                                                          .setStoreAddress("store-address");

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbName()).thenReturn(dbName);
        when(propertyServiceMock.getCategory()).thenReturn(category);
        when(propertyServiceMock.getOutFormat()).thenReturn(outFormat);
        when(mongoDaoMock.getStoreWithMaxNumberOfProductsByCategory(category)).thenReturn(storeWithProductCategory);

        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(dbUrl))
                    .thenReturn(mongoClientMock);
            when(daoFactoryMock.getMongoDao(mongoClientMock, dbName, converterMock))
                    .thenReturn(mongoDaoMock);
            when(executorCreatorMock.createGeneratorValidatorExecutor(anyInt(),
                    anyInt(),
                    any(),
                    any(ProductInStoreGenerator.class),
                    any(ProductInStoreValidator.class)))
                    .thenReturn(executorServiceWithGeneratorsMock);
            when(executorCreatorMock.createBatchSenderExecutor(anyInt(),
                    any(),
                    any(DaoFactory.class),
                    anyString(),
                    anyString(),
                    anyInt()))
                    .thenReturn(executorServiceWithBatchSendersMock);

            when(executorServiceWithGeneratorsMock.awaitTermination(anyLong(), any())).thenThrow(InterruptedException.class);
            when(executorServiceWithBatchSendersMock.awaitTermination(anyLong(), any())).thenReturn(false);

            app.run();

            verify(executorServiceWithGeneratorsMock, times(1)).shutdownNow();
            verify(executorServiceWithBatchSendersMock, times(1)).shutdownNow();
        }
    }
}