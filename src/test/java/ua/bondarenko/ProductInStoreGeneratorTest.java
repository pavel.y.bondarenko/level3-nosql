package ua.bondarenko;

import org.bson.BsonValue;
import ua.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.bondarenko.ProductInStoreGenerator;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreGeneratorTest {
    private static final int MAX_AMOUNT = 100;
    private ProductInStoreGenerator productInStoreGenerator;

    @Mock
    private Random randomMock;

    @Mock
    private BsonValue productIdMock1;

    @Mock
    private BsonValue productIdMock2;

    @Mock
    private BsonValue productIdMock3;

    @Mock
    private BsonValue storeIdMock1;

    @Mock
    private BsonValue storeIdMock2;

    private List<BsonValue> productIds;
    private List<BsonValue> storeIds;

    @BeforeEach
    void setUp() {
        productIds = List.of(productIdMock1, productIdMock2, productIdMock3);
        storeIds = List.of(storeIdMock1, storeIdMock2);
        productInStoreGenerator = new ProductInStoreGenerator(storeIds, productIds, randomMock, MAX_AMOUNT);
    }

    @AfterEach
    void tearDown() {
        productInStoreGenerator = null;
    }

    @Test
    void generateTest() {
        int storeIndex = 0;
        int productIndex = 1;
        int amount = 57;

        Mockito.when(randomMock.ints(0, storeIds.size()))
               .thenReturn(IntStream.of(storeIndex));
        Mockito.when(randomMock.ints(0, productIds.size()))
               .thenReturn(IntStream.of(productIndex));
        Mockito.when(randomMock.ints(1, MAX_AMOUNT + 1))
               .thenReturn(IntStream.of(amount));

        ProductInStore productInStore = productInStoreGenerator.generate();
        assertEquals(storeIdMock1, productInStore.getStoreId());
        assertEquals(productIdMock2, productInStore.getProductId());
        assertEquals(amount, productInStore.getAmount());
    }
}