package ua.bondarenko;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.BsonValue;
import ua.bondarenko.converter.ProductInStoreToDocumentConverter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.dao.MongoDao;
import ua.bondarenko.exception.DatabaseConnectionException;
import ua.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreBatchSenderTest {
    private static final String DB_URL = "url";
    private static final String DB_NAME = "name";
    private ProductInStoreBatchSender productInStoreBatchSender;

    private final BlockingQueue<ProductInStore> productInStoreQueue = new LinkedBlockingQueue<>();

    @Mock
    private MongoDao mongoDaoMock;

    @Mock
    private MongoClient mongoClientMock;

    @Mock
    private DaoFactory daoFactoryMock;

    @Mock
    private BsonValue storeIdMock;

    @Mock
    private BsonValue productIdMock;

    @BeforeEach
    void setUp() {
        int batchMaxSize = 2;
        productInStoreBatchSender = new ProductInStoreBatchSender(productInStoreQueue,
                daoFactoryMock,
                DB_URL,
                DB_NAME,
                batchMaxSize);


        ProductInStore productInStore = new ProductInStore().setStoreId(storeIdMock)
                .setProductId(productIdMock)
                .setAmount(5);
        ProductInStore poisonProductInStore = new ProductInStore();

        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(poisonProductInStore);
    }

    @AfterEach
    void tearDown() {
        productInStoreBatchSender = null;
    }

    @Test
    void runTest() {
        int numberOfBatches = 3;
        when(daoFactoryMock.getMongoDao(eq(mongoClientMock), eq(DB_NAME), any(ProductInStoreToDocumentConverter.class)))
                .thenReturn(mongoDaoMock);

        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(DB_URL))
                    .thenReturn(mongoClientMock);
            productInStoreBatchSender.run();
        }

        verify(mongoDaoMock, times(numberOfBatches)).addAllProductInStore(any());
    }

    @Test
    void runWithExceptionTest() {
        when(daoFactoryMock.getMongoDao(eq(mongoClientMock), eq(DB_NAME), any(ProductInStoreToDocumentConverter.class)))
                           .thenReturn(mongoDaoMock);
        when(mongoDaoMock.addAllProductInStore(any())).thenThrow(MongoException.class);
        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(DB_URL))
                    .thenReturn(mongoClientMock);
            assertDoesNotThrow(() -> productInStoreBatchSender.run());
        }
    }

    @Test
    void runWithExceptionInConnectionTest() {
        try (MockedStatic<MongoClients> managerMockedStatic = mockStatic(MongoClients.class)) {
            managerMockedStatic.when(() -> MongoClients.create(DB_URL))
                    .thenThrow(IllegalStateException.class);
            assertDoesNotThrow(() -> productInStoreBatchSender.run());
        }
    }
}