package ua.bondarenko;

import ua.bondarenko.exception.PropertyFileDoesNotExistsException;
import ua.bondarenko.exception.PropertyMissedException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class PropertyServiceTest {
    private static final String VALID_PROPERTY_FILE_NAME = "valid.properties";
    private static final String URL_MISSED_PROPERTY_FILE_NAME = "url-missed.properties";
    private static final String DBNAME_MISSED_PROPERTY_FILE_NAME = "dbName-missed.properties";
    private static final String NOT_EXISTS_PROPERTY_FILE_NAME = "not-exists.properties";
    private static final String CATEGORY_PROPERTY_NAME = "category";
    private static final String CATEGORY_VALUE = "some category";
    private static final String URL_VALUE = "url";
    private static final String DBNAME_VALUE = "dbname";
    private static final int NUMBER_OF_CATEGORIES = 1_111;
    private static final int NUMBER_OF_PRODUCTS_PER_CATEGORY = 2_222;
    private static final int NUMBER_OF_STORES = 3_333;
    private static final int NUMBER_OF_PRODUCT_IN_STORE = 4_444;
    private static final int BATCH_MAX_SIZE = 5_555;
    private static final int MAX_AMOUNT_OF_PRODUCTS = 6_666;
    private static final int NUMBER_OF_GENERATORS = 7_777;
    private static final int NUMBER_OF_SENDERS = 8_888;
    private static final int MAX_WAITING_TIME_IN_SECONDS = 9_999;
    private static final String OUT_FORMAT = "format";
    private static PropertyService propertyService;

    @BeforeAll
    static void setAll() {
        System.setProperty(CATEGORY_PROPERTY_NAME, CATEGORY_VALUE);
        propertyService = new PropertyService(VALID_PROPERTY_FILE_NAME);
    }

    @AfterAll
    static void clearAll() {
        propertyService = null;
    }

    @BeforeEach
    void setUp() {
        System.setProperty(CATEGORY_PROPERTY_NAME, CATEGORY_VALUE);
    }

    @Test
    void getDbUrlTest() {
        assertEquals(URL_VALUE, propertyService.getDbUrl());
    }

    @Test
    void getDbnameTest() {
        assertEquals(DBNAME_VALUE, propertyService.getDbName());
    }

    @Test
    void getCategoryTest() {
        assertEquals(CATEGORY_VALUE, propertyService.getCategory());
    }

    @Test
    void getNumberOfCategoriesTest() {
        assertEquals(NUMBER_OF_CATEGORIES, propertyService.getNumberOfCategories());
    }

    @Test
    void getNumberOfProductsPerCategoryTest() {
        assertEquals(NUMBER_OF_PRODUCTS_PER_CATEGORY, propertyService.getNumberOfProductsPerCategory());
    }

    @Test
    void getNumberOfStoresTest() {
        assertEquals(NUMBER_OF_STORES, propertyService.getNumberOfStores());
    }

    @Test
    void getNumberOfProductInStoreTest() {
        assertEquals(NUMBER_OF_PRODUCT_IN_STORE, propertyService.getNumberOfProductInStore());
    }

    @Test
    void getBatchMaxSizeTest() {
        assertEquals(BATCH_MAX_SIZE, propertyService.getBatchMaxSize());
    }

    @Test
    void getMaxAmountOfProductsTest() {
        assertEquals(MAX_AMOUNT_OF_PRODUCTS, propertyService.getMaxAmountOfProducts());
    }

    @Test
    void getNumberOfGeneratorsTest() {
        assertEquals(NUMBER_OF_GENERATORS, propertyService.getNumberOfGenerators());
    }

    @Test
    void getNumberOfSendersTest() {
        assertEquals(NUMBER_OF_SENDERS, propertyService.getNumberOfSenders());
    }

    @Test
    void getMaxWaitingTimeTest() {
        assertEquals(MAX_WAITING_TIME_IN_SECONDS, propertyService.getMaxWaitingTimeInSeconds());
    }

    @Test
    void getOutFormatTest() {
        assertEquals(OUT_FORMAT, propertyService.getOutFormat());
    }

    @Test
    void missedCategoryPropertyTest() {
        System.clearProperty(CATEGORY_PROPERTY_NAME);
        assertThrows(PropertyMissedException.class, () -> new PropertyService(VALID_PROPERTY_FILE_NAME));
    }

    @Test
    void missedUrlPropertyTest() {
        assertThrows(PropertyMissedException.class, () -> new PropertyService(URL_MISSED_PROPERTY_FILE_NAME));
    }

    @Test
    void missedDbnamePropertyTest() {
        assertThrows(PropertyMissedException.class, () -> new PropertyService(DBNAME_MISSED_PROPERTY_FILE_NAME));
    }

    @Test
    void notExistsPropertyFileTest() {
        assertThrows(PropertyFileDoesNotExistsException.class, () -> new PropertyService(NOT_EXISTS_PROPERTY_FILE_NAME));
    }
}