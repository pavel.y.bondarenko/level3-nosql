package ua.bondarenko;

import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExecutorCreatorTest {
    private ExecutorCreator executorCreator;
    private BlockingQueue<ProductInStore> queue = new LinkedBlockingQueue<>();

    @Mock
    private ExecutorService executorServiceMock;

    @Mock
    private DaoFactory daoFactoryMock;

    @Mock
    private ProductInStoreGenerator generatorMock;

    @Mock
    private ProductInStoreValidator validatorMock;

    @BeforeEach
    void setUp() {
        executorCreator = new ExecutorCreator();
    }

    @AfterEach
    void tearDown() {
        executorCreator = null;
    }

    @Test
    void createGeneratorValidatorExecutorTest() {
        int numberOfThreads = 5;
        try (MockedStatic<Executors> managerMockedStatic = mockStatic(Executors.class)) {
            managerMockedStatic.when(() -> Executors.newFixedThreadPool(numberOfThreads))
                               .thenReturn(executorServiceMock);
            assertEquals(executorServiceMock, executorCreator.createGeneratorValidatorExecutor(numberOfThreads,
                                                                                               numberOfThreads,
                                                                                               queue,
                                                                                               generatorMock,
                                                                                               validatorMock));
        }
        verify(executorServiceMock, times(numberOfThreads)).execute(any(ProductInStoreValidateGenerator.class));
    }

    @Test
    void createBatchSenderExecutor() {
        int numberOfThreads = 5;
        int batchMaxSize = 10;
        try (MockedStatic<Executors> managerMockedStatic = mockStatic(Executors.class)) {
            managerMockedStatic.when(() -> Executors.newFixedThreadPool(numberOfThreads))
                               .thenReturn(executorServiceMock);
            assertEquals(executorServiceMock, executorCreator.createBatchSenderExecutor(numberOfThreads,
                                                                                        queue,
                                                                                        daoFactoryMock,
                                                                                        "",
                                                                                        "",
                                                                                        batchMaxSize));
        }
        verify(executorServiceMock, times(numberOfThreads)).execute(any(ProductInStoreBatchSender.class));
    }
}