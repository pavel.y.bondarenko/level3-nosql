package ua.bondarenko;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import ua.bondarenko.converter.Converter;
import ua.bondarenko.dao.DaoFactory;
import ua.bondarenko.exception.DatabaseConnectionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.bondarenko.pojo.ProductInStore;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DaoFactoryTest {

    private DaoFactory daoFactory;

    private final String dbName = "name";

    @Mock
    private MongoClient mongoClientMock;

    @Mock
    private Converter<ProductInStore, Document> converterMock;

    @Mock
    private MongoDatabase mongoDatabaseMock;

    @BeforeEach
    void beforeEach() {
        daoFactory = new DaoFactory();
    }

    @AfterEach
    void afterEach() {
        daoFactory = null;
    }

    @Test
    void getMongoDaoCorrectTest() {
        when(mongoClientMock.getDatabase(dbName)).thenReturn(mongoDatabaseMock);
        daoFactory.getMongoDao(mongoClientMock, dbName, converterMock);
        verify(mongoClientMock, times(1)).getDatabase(dbName);
        verify(mongoDatabaseMock, times(4)).getCollection(anyString());
    }

    @Test
    void getMongoDaoClientIsNullTest() {
        assertThrows(DatabaseConnectionException.class, () -> daoFactory.getMongoDao(null, dbName, converterMock));
    }

    @Test
    void getMongoDaoDbNameIsNullTest() {
        assertThrows(DatabaseConnectionException.class, () -> daoFactory.getMongoDao(mongoClientMock, null, converterMock));
    }

    @Test
    void getMongoDaoConverterIsNullTest() {
        assertThrows(IllegalArgumentException.class, () -> daoFactory.getMongoDao(mongoClientMock, dbName, null));
    }
}